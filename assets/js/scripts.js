/*!
 * template-name
 * Template Title
 * https://a-web.org/
 * @author AWEB
 * @version 1.0.0
 * Copyright 2018. MIT licensed.
 */
/*Custom Scripts*/
(function($) {
    'use strict';

    jQuery(document).ready(function() {

    	//Calender Slider
    	var $calender_slider = $('.calender-slider');
		$calender_slider.slick({
		  centerMode: true,
		  slidesToShow: 1,
		  centerPadding: '30px',
		  prevArrow: '.prev',
       	  nextArrow: '.next',
       	  mobileFirst:true,
		  responsive: [
		  	{
		      breakpoint: 1023,
		      settings: {
		        centerPadding: '0px',
		        slidesToShow: 3
		      }
		    },
		  	{
		      breakpoint: 767,
		      settings: {
		        centerPadding: '120px',
		        slidesToShow: 1
		      }
		    },
		  ]
		});

 		$('a.first').click(function(e) {
 		  	$calender_slider.slick('slickGoTo', 0 );
 		});

    	$calender_slider.on('init reInit afterChange', function (event, slick) {
    		var total = slick.slideCount-1;
        	//console.log(total);
	        $('a.last').click(function(e) {
	 		  	$calender_slider.slick('slickGoTo', total );
	 		});
    	});

    	/*Calander arrow z-index*/
    	$('.search-slider-area .slick-active.slick-center .asr-calender,li.space').live('hover', function(){
    		$('.slider-arrows').toggleClass('hover');
    	})
 		
 		/**
 		 * Footer Navigation url replace from comic posts
 		 **/
 		var prev_post = $('.js-hook .navi-prev').attr('href');
 		$('a.prev-comic').prop('href', prev_post);
 		//console.log('Prev Comic: '+prev_post);

 		var next_post = $('.js-hook .navi-next').attr('href');
 		$('a.next-comic').prop('href', next_post);
 		//console.log('Next Comic: '+next_post);

 		//CLick to collapse share bar
 		$('.foo-nav').on('click', function(){
 			$('#foo-share').removeClass('in');
 			$('a.share').addClass('collapsed');
 		});
		
		//CLick to collapse hambarg menu
 		$('a.share').on('click', function(){
 			$('#humbarger').removeClass('in');
 			$('a.foo-nav').addClass('collapsed');
 		});
    });

})(jQuery);